package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;
import at.campus02.dbp2.flights.domain.SeatType;
import org.junit.After;
import org.junit.Before;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class BaseSpecification {

    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;
    FlightsDao flightsDao;

    // <editor-fold desc="Common Testdata">
    final String email = "email.address@mail.com";
    final String lastname = "Lastname";
    final String firstname = "Firstname";
    final String alternativeLastname = "Anotherlastname";

    final String graz = "Graz";
    final String wien = "Wien";

    final String flightNr1 = "OS 172";
    final String flightNr2 = "FX 5112";
    final String flightNr3 = "LN 4332";

    Flight flight1;
    Flight flight2;
    Flight flight3;

    Customer customer1;
    Customer customer2;
    Customer customer3;

    Seat seat1a;
    Seat seat1b;
    Seat seat2a;
    Seat seat2b;
    Seat seat2c;
    Seat seat2d;
    Seat seat3a;
    Seat seat3b;

    final Set<Seat> availableSeats = new HashSet<>();
    final Set<Seat> availableSeatsToGraz = new HashSet<>();
    final Set<Seat> availableSeatsToWien = new HashSet<>();
    final Set<Seat> seatsReservedByCustomer1 = new HashSet<>();
    final Set<Seat> seatsReservedByCustomer2 = new HashSet<>();

    void setUpTestData() {

        entityManager.getTransaction().begin();

        // Flights...
        flight1 = prepareFlight(flightNr1, graz);
        flight2 = prepareFlight(flightNr2, graz);
        flight3 = prepareFlight(flightNr3, wien);
        entityManager.persist(flight1);
        entityManager.persist(flight2);
        entityManager.persist(flight3);

        // Customers...
        customer1 = prepareCustomer("Huber", "Hansi", "hansi.huber@email.com");
        customer2 = prepareCustomer("Huber", "Hannelore", "lore.huber@email.com");
        customer3 = prepareCustomer("Klingenschmidt", "Konrad", "konrad.k@email.com");
        entityManager.persist(customer1);
        entityManager.persist(customer2);
        entityManager.persist(customer3);

        // Seats...
        seat1a = prepareSeat("12A", SeatType.WINDOW);
        seat1a.setFlight(flight1);
        flight1.getSeats().add(seat1a);
        seat1a.setCustomer(customer1); // occupied by customer 1
        entityManager.persist(seat1a);

        seat1b = prepareSeat("12B", SeatType.MIDDLE);
        seat1b.setFlight(flight1);
        flight1.getSeats().add(seat1b);
        entityManager.persist(seat1b);

        seat2a = prepareSeat("14D", SeatType.AISLE);
        seat2a.setFlight(flight2);
        flight2.getSeats().add(seat2a);
        seat2a.setCustomer(customer1); // occupied by customer 1
        entityManager.persist(seat2a);

        seat2b = prepareSeat("17C", SeatType.AISLE);
        seat2b.setFlight(flight2);
        flight2.getSeats().add(seat2b);
        seat2b.setCustomer(customer3); // occupied by customer 3
        entityManager.persist(seat2b);

        seat2c = prepareSeat("21A", SeatType.WINDOW);
        seat2c.setFlight(flight2);
        flight2.getSeats().add(seat2c);
        seat2c.setCustomer(customer2); // occupied by customer 2
        entityManager.persist(seat2c);

        seat2d = prepareSeat("1A", SeatType.WINDOW);
        seat2d.setFlight(flight2);
        flight2.getSeats().add(seat2d);
        entityManager.persist(seat2d);

        seat3a = prepareSeat("12E", SeatType.MIDDLE);
        seat3a.setFlight(flight3);
        flight3.getSeats().add(seat3a);
        seat3a.setCustomer(customer2); // occupied by customer 2
        entityManager.persist(seat3a);

        seat3b = prepareSeat("12A", SeatType.WINDOW);
        seat3b.setFlight(flight3);
        flight3.getSeats().add(seat3b);
        entityManager.persist(seat3b);

        entityManager.getTransaction().commit();

        // test sets
        availableSeatsToGraz.addAll(Arrays.asList(seat1b, seat2d));
        availableSeatsToWien.addAll(Collections.singletonList(seat3b));
        availableSeats.addAll(availableSeatsToGraz);
        availableSeats.addAll(availableSeatsToWien);
        seatsReservedByCustomer1.addAll(Arrays.asList(seat1a, seat2a));
        seatsReservedByCustomer2.addAll(Arrays.asList(seat2c, seat3a));
    }    // </editor-fold>

    @Before
    public void before() {
        entityManagerFactory = Persistence.createEntityManagerFactory("assignment");
        entityManager = entityManagerFactory.createEntityManager();
        FlightsDaoFactory daoFactory = new FlightsDaoFactory(entityManagerFactory);
        flightsDao = daoFactory.getFlightsDao();
    }

    @After
    public void after() {
        if (flightsDao != null) {
            flightsDao.close();
        }
        if (entityManager != null && entityManager.isOpen()) {
            entityManager.close();
        }
        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }

    static Customer prepareCustomer(String lastname, String firstname, String email) {
        Customer customer = new Customer();
        customer.setEmail(email);
        customer.setLastname(lastname);
        customer.setFirstname(firstname);
        return customer;
    }

    static Flight prepareFlight(String flightNumber, String destination) {
        Flight flight = new Flight();
        flight.setFlightNumber(flightNumber);
        flight.setDestination(destination);
        return flight;
    }

    static Seat prepareSeat(String seatNumber, SeatType type) {
        Seat seat = new Seat();
        seat.setSeatNumber(seatNumber);
        seat.setType(type);
        return seat;
    }

    <T> EntityManager getEntityManagerFromInterface(T manager) throws IllegalAccessException {
        EntityManager em = null;
        Field[] fields = manager.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.getType().isAssignableFrom(EntityManager.class)) {
                field.setAccessible(true);
                em = (EntityManager) field.get(manager);
                break;
            }
        }
        return em;
    }

}
