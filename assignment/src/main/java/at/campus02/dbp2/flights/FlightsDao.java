package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;

import java.util.List;

public interface FlightsDao {

    // CRUD: flights
    boolean create(Flight flight);
    Flight readFlight(String flightNumber);
    Flight update(Flight flight);
    boolean delete(Flight flight);

    // CRUD: customers
    boolean create(Customer customer);
    Customer readCustomer(String email);
    Customer update(Customer customer);
    boolean delete(Customer customer);

    // Suche: flights
    List<Flight> findFlightsByDestination(String destination);

    // Suche: customers
    List<Customer> findCustomersBy(String lastname, String firstname);

    // Suche: seats
    List<Seat> findAvailableSeatsTo(String destination);
    List<Seat> findSeatsBookedBy(Customer customer);

    // Reservierung / Cancel
    boolean reserve(Seat seat, Customer customer);
    boolean cancel(Seat seat, Customer customer);

    // Resource management
    void close();

}
