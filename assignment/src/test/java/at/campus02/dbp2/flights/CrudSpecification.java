package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;
import at.campus02.dbp2.flights.domain.SeatType;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class CrudSpecification extends BaseSpecification {


    // <editor-fold desc="Customer">

    // <editor-fold desc="create Customer">
    @Test
    public void createCustomerPersistsInDatabase() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, email);

        // when
        boolean success = flightsDao.create(customer);
        String id = customer.getEmail();
        entityManager.clear();

        // then
        assertThat(success, is(true));
        assertThat(id, is(email));
        assertThat(entityManager.find(Customer.class, id).getEmail(), is(email));
        assertThat(entityManager.find(Customer.class, id).getLastname(), is(lastname));
        assertThat(entityManager.find(Customer.class, id).getFirstname(), is(firstname));
    }

    @Test
    public void createCustomerWithExistingEmailReturnsFalse() {
        // given
        Customer customer1 = prepareCustomer(lastname, firstname, email);
        Customer customer2 = prepareCustomer(alternativeLastname, firstname, email);
        flightsDao.create(customer1);

        // when
        boolean success = flightsDao.create(customer2);

        // then
        assertThat(success, is(false));
        assertThat(entityManager.find(Customer.class, email).getLastname(), is(lastname));
    }

    @Test
    public void createNullAsCustomerReturnsFalse() {
        // when
        Customer customer = null;
        boolean success = flightsDao.create(customer);

        // then
        assertThat(success, is(false));
    }

    @Test
    public void createCustomerWithoutEmailReturnsFalse() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, null);

        // when
        boolean success = flightsDao.create(customer);

        // then
        assertThat(success, is(false));
        assertThat(entityManager
                .createQuery("select c from Customer c where c.firstname=:firstname and c.lastname=:lastname",
                        Customer.class)
                .setParameter("firstname", firstname)
                .setParameter("lastname", lastname)
                .getResultList().isEmpty(), is(true));
    }
    // </editor-fold>

    // <editor-fold desc="delete Customer">
    @Test
    public void deleteCustomerRemovesFromDatabase() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, email);
        flightsDao.create(customer);

        // when
        boolean success = flightsDao.delete(customer);
        String id = customer.getEmail();

        // then
        assertThat(success, is(true));
        assertThat(entityManager.find(Customer.class, id), is(nullValue()));
    }

    @Test
    public void deleteNotExistingCustomerReturnsFalse() {
        // given
        Customer customer1 = prepareCustomer(lastname, firstname, email);
        Customer customer2 = prepareCustomer(lastname, firstname, null);

        // when
        boolean success = flightsDao.delete(customer1);
        String id = customer1.getEmail();

        // then
        assertThat(success, is(false));
        assertThat(entityManager.find(Customer.class, id), is(nullValue()));

        // and when
        success = flightsDao.delete(customer2);

        // then
        assertThat(success, is(false));
    }

    // </editor-fold>

    // <editor-fold desc="read Customer">
    @Test
    public void readCustomerFindsItInDatabase() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, email);
        flightsDao.create(customer);

        // when
        Customer found = flightsDao.readCustomer(customer.getEmail());
        String id = customer.getEmail();

        // then
        assertThat(entityManager.find(Customer.class, id), is(found));
        assertThat(found, is(customer));
        assertThat(found.getEmail(), is(customer.getEmail()));
        assertThat(found.getFirstname(), is(customer.getFirstname()));
        assertThat(found.getLastname(), is(customer.getLastname()));
    }

    @Test
    public void readCustomerWithNullAsEmailReturnsNull() {
        // when
        String email = null;
        Customer found = flightsDao.readCustomer(email);

        // then
        assertThat(found, is(nullValue()));
    }
    // </editor-fold>

    // <editor-fold desc="update Customer">
    @Test
    public void updateCustomerChangesValuesInDatabase() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, email);
        flightsDao.create(customer);

        // when
        customer.setLastname(alternativeLastname);
        Customer updated = flightsDao.update(customer);
        String id = customer.getEmail();

        // then
        assertThat(entityManager.find(Customer.class, id), is(updated));
        assertThat(updated, is(customer));
        assertThat(updated.getEmail(), is(email));
        assertThat(updated.getFirstname(), is(firstname));
        assertThat(updated.getLastname(), is(alternativeLastname));
        assertThat(customer.getLastname(), is(alternativeLastname));
    }

    @Test
    public void updateNotExistingCustomerReturnsNull() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, email);

        // when
        Customer updated = flightsDao.update(customer);

        // then
        assertThat(updated, is(nullValue()));
        assertThat(customer.getEmail(), is(email));
        assertThat(customer.getFirstname(), is(firstname));
        assertThat(customer.getLastname(), is(lastname));

        // and when
        customer = null;
        updated = flightsDao.update(customer);
        assertThat(updated, is(nullValue()));
    }
    // </editor-fold>
    // </editor-fold>

    // <editor-fold desc="Flight">

    // <editor-fold desc="create Flight">
    @Test
    public void createFlightPersistsFlightInDatabase() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);

        // when
        boolean success = flightsDao.create(flight);
        String flightNr = flight.getFlightNumber();

        // then
        assertThat(success, is(true));
        assertThat(entityManager.find(Flight.class, flightNr), is(notNullValue()));
        assertThat(entityManager.find(Flight.class, flightNr).getDestination(), is(graz));
    }

    @Test
    public void createFlightThatAlreadyExistsReturnsFalse() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        flightsDao.create(flight);

        // when
        boolean success = flightsDao.create(flight);

        // then
        assertThat(success, is(false));
        assertThat(entityManager.find(Flight.class, flight.getFlightNumber()).getFlightNumber(), is(flightNr1));
        assertThat(entityManager.find(Flight.class, flight.getFlightNumber()).getDestination(), is(graz));
    }

    @Test
    public void createNullAsFlightReturnsFalse() {
        // when
        Flight flight = null;
        boolean success = flightsDao.create(flight);

        // then
        assertThat(success, is(false));
    }

    @Test
    public void createFlightPersistsAlsoSeatsInDatabase() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        Seat seat = prepareSeat("12A", SeatType.WINDOW);
        flight.getSeats().add(seat);

        // when
        boolean success = flightsDao.create(flight);
        String flightNumber = flight.getFlightNumber();

        // then
        assertThat(success, is(true));
        assertThat(entityManager.find(Flight.class, flightNumber).getSeats().contains(seat), is(true));
        assertThat(
                entityManager.createQuery("select s from Seat s where s.flight.flightNumber=:flightNumber", Seat.class)
                        .setParameter("flightNumber", flightNumber).getResultList().size(), is(1));
    }
    // </editor-fold>

    // <editor-fold desc="delete Flight">
    @Test
    public void deleteFlightRemovesFromDatabase() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        flightsDao.create(flight);
        String flightNumber = flight.getFlightNumber();

        // when
        boolean success = flightsDao.delete(flight);

        // then
        assertThat(success, is(true));
        assertThat(entityManager.find(Flight.class, flightNumber), is(nullValue()));
    }

    @Test
    public void deleteNotExistingFlightReturnsFalse() {
        // given
        Flight flight1 = prepareFlight(flightNr1, graz);
        Flight flight2 = prepareFlight(null, null);

        // when
        boolean success = flightsDao.delete(flight1);

        // then
        assertThat(success, is(false));

        // and when
        success = flightsDao.delete(flight2);

        // then
        assertThat(success, is(false));
    }
    // </editor-fold>

    // <editor-fold desc="read Flight">
    @Test
    public void readFlightFindsItInDatabase() {
        // given
        Flight flight = prepareFlight("12A", graz);
        flightsDao.create(flight);

        // when
        String flightNumber = flight.getFlightNumber();
        Flight found = flightsDao.readFlight(flightNumber);

        // then
        assertThat(entityManager.find(Flight.class, flightNumber), is(found));
        assertThat(found, is(flight));
        assertThat(found.getDestination(), is(flight.getDestination()));
        assertThat(found.getFlightNumber(), is(flight.getFlightNumber()));
    }

    @Test
    public void readFlightWithNullAsIdReturnsNull() {
        // when
        String flightNumber = null;
        Flight found = flightsDao.readFlight(flightNumber);

        // then
        assertThat(found, is(nullValue()));
    }

    @Test
    public void readFlightFindsAlsoSeatsInDatabase() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        Seat seat1 = prepareSeat("12A", SeatType.WINDOW);
        Seat seat2 = prepareSeat("14C", SeatType.AISLE);
        flight.getSeats().add(seat1);
        flight.getSeats().add(seat2);
        flightsDao.create(flight);

        // when
        String flightNumber = flight.getFlightNumber();
        Flight found = flightsDao.readFlight(flightNumber);
        Flight fromDb = entityManager.find(Flight.class, flightNumber);

        // then
        assertThat(found, is(fromDb));
        assertThat(found, is(flight));
        assertThat(found.getSeats(), is(Arrays.asList(seat1, seat2)));
        assertThat(fromDb.getSeats(), is(Arrays.asList(seat1, seat2)));
    }
    // </editor-fold>

    // <editor-fold desc="update Flight">
    @Test
    public void updateFlightChangesValuesInDatabase() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        flightsDao.create(flight);

        // when
        flight.setDestination(wien);
        Flight updated = flightsDao.update(flight);
        Flight fromDb = entityManager.find(Flight.class, flight.getFlightNumber());

        // then
        assertThat(updated, is(fromDb));
        assertThat(updated, is(flight));
        assertThat(updated.getDestination(), is(wien));
    }
    @Test
    public void updateFlightStoresAllSeatsNotYetPersisted() {
        // given
        Flight flight = prepareFlight(flightNr1, "Graz");
        Seat seat1 = prepareSeat("12A", SeatType.WINDOW);
        Seat seat2 = prepareSeat("14C", SeatType.AISLE);
        Seat seat3 = prepareSeat("17E", SeatType.MIDDLE);
        flight.getSeats().add(seat1);
        flightsDao.create(flight);

        // when
        flight.getSeats().add(seat2);
        flight.getSeats().add(seat3);
        Flight updated = flightsDao.update(flight);

        // then
        assertThat(updated, is(flight));
        assertThat(updated.getSeats().size(), is(3));
        assertThat(updated.getSeats().containsAll(Arrays.asList(seat1, seat2, seat3)), is(true));
        assertThat(seat1.getId(), is(not(nullValue())));
        assertThat(seat1.getFlight(), is(flight));
        assertThat(seat2.getId(), is(not(nullValue())));
        assertThat(seat2.getFlight(), is(flight));
        assertThat(seat3.getId(), is(not(nullValue())));
        assertThat(seat3.getFlight(), is(flight));
    }
    @Test
    public void updateFlightAlsoUpdatesSeatValues() {
        // given
        Flight car = prepareFlight(flightNr1, graz);
        Seat seat1 = prepareSeat("12A", SeatType.WINDOW);
        Seat seat2 = prepareSeat("14C", SeatType.AISLE);
        Seat seat3 = prepareSeat("17E", SeatType.MIDDLE);
        car.getSeats().add(seat1);
        car.getSeats().add(seat2);
        car.getSeats().add(seat3);
        flightsDao.create(car);

        // when
        seat2.setType(SeatType.WINDOW);
        Flight updated = flightsDao.update(car);

        // then
        assertThat(updated, is(car));
        assertThat(updated.getSeats().contains(seat2), is(true));
        assertThat(entityManager.find(Seat.class, seat2.getId()).getType(), is(SeatType.WINDOW));
    }
    // </editor-fold>
    // </editor-fold>

    // <editor-fold desc="close">
    @Test
    public void closeClosesEntityManagerButDoesNotCloseFactory() throws IllegalAccessException {
        // given
        EntityManager em = getEntityManagerFromInterface(flightsDao);

        // when
        flightsDao.close();

        // then
        if (em != null) {
            assertThat(em.isOpen(), is(false));
        }
        assertThat(entityManagerFactory.isOpen(), is(true));
    }
    // </editor-fold>

}
