package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;
import at.campus02.dbp2.flights.domain.SeatType;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;


public class FlightsSpecification extends BaseSpecification {

    // <editor-fold desc="findFlights">
    @Test
    public void findFlightsByDestinationFindsAllFlightsWithThatDestination() {
        // given
        setUpTestData();

        // when
        List<Flight> flights = flightsDao.findFlightsByDestination(graz);

        // then
        assertThat(flights.size(), is(2));
        assertThat(flights.contains(flight1), is(true));
        assertThat(flights.contains(flight2), is(true));

        // and when
        flights = flightsDao.findFlightsByDestination(wien);
        assertThat(flights.size(), is(1));
        assertThat(flights.contains(flight3), is(true));
    }
    @Test
    public void findFlightsByNullReturnsAllFlights() {
        // given
        setUpTestData();

        // when
        List<Flight> flights = flightsDao.findFlightsByDestination(null);

        // then
        assertThat(flights.size(), is(3));
        assertThat(flights.contains(flight1), is(true));
        assertThat(flights.contains(flight2), is(true));
        assertThat(flights.contains(flight3), is(true));
    }
    // </editor-fold>
    // <editor-fold desc="findCustomers">
    @Test
    public void findCustomerByLastnameFindsMatchingCustomers() {
        // given
        setUpTestData();

        // when
        List<Customer> customers = flightsDao.findCustomersBy("Huber", null);

        // then
        assertThat(customers.size(), is(2));
        assertThat(customers.contains(customer1), is(true));
        assertThat(customers.contains(customer2), is(true));
    }
    @Test
    public void findCustomerByFirstnameFindsMatchingCustomers() {
        // given
        setUpTestData();

        // when
        List<Customer> customers = flightsDao.findCustomersBy(null, "Konrad");

        // then
        assertThat(customers.size(), is(1));
        assertThat(customers.contains(customer3), is(true));
    }
    @Test
    public void findCustomerByLastnameAndFirstnameFindsMatchingCustomers() {
        // given
        setUpTestData();

        // when
        List<Customer> customers = flightsDao.findCustomersBy("Huber", "Hansi");

        // then
        assertThat(customers.size(), is(1));
        assertThat(customers.contains(customer1), is(true));
    }
    // </editor-fold>

    // <editor-fold desc="findAvailableSeatsTo">
    @Test
    public void findAvailableSeatsToDestinationFindsAvailableSeatsOfAllFlightsWithThatDestination() {
        // given
        setUpTestData();

        // when
        List<Seat> seats = flightsDao.findAvailableSeatsTo(graz);

        // then
        assertThat(seats.size(), is(availableSeatsToGraz.size()));
        assertThat(seats.containsAll(availableSeatsToGraz), is(true));

        // and when
        seats = flightsDao.findAvailableSeatsTo(wien);

        // then
        assertThat(seats.size(), is(availableSeatsToWien.size()));
        assertThat(seats.containsAll(availableSeatsToWien), is(true));
    }
    @Test
    public void findAvailableSeatsToUnknownDestinationReturnsEmptyList() {
        // given
        setUpTestData();

        // when
        List<Seat> seats = flightsDao.findAvailableSeatsTo("New York");

        // then
        assertThat(seats.isEmpty(), is(true));
    }
    // </editor-fold>

    // <editor-fold desc="findSeatsReservedFor">
    @Test
    public void findSeatsBookedByCustomerFindsSeatsReservedByCustomer() {
        // given
        setUpTestData();

        // when
        List<Seat> seats = flightsDao.findSeatsBookedBy(customer1);

        // then
        assertThat(seats.size(), is(seatsReservedByCustomer1.size()));
        assertThat(seats.containsAll(seatsReservedByCustomer1), is(true));

        // and when
        seats = flightsDao.findSeatsBookedBy(customer2);

        // then
        assertThat(seats.size(), is(seatsReservedByCustomer2.size()));
        assertThat(seats.containsAll(seatsReservedByCustomer2), is(true));
    }
    @Test
    public void findSeatsBookedByUnknownCustomerReturnsEmptyList() {
        // given
        setUpTestData();

        // when
        List<Seat> seats = flightsDao.findSeatsBookedBy(null);

        // then
        assertThat(seats.isEmpty(), is(true));

        // when
        seats = flightsDao.findSeatsBookedBy(prepareCustomer("Lastname", "Firstname", "email"));

        // then
        assertThat(seats.isEmpty(), is(true));
    }
    // </editor-fold>
    // <editor-fold desc="reserve">
    @Test
    public void reserveAssignsCustomerToSeatAndReturnsTrue() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.reserve(seat1b, customer3);

        // then
        assertThat(success, is(true));
        assertThat(seat1b.getCustomer().getEmail(), is(customer3.getEmail()));
        assertThat(seat1b.getCustomer().getLastname(), is(customer3.getLastname()));
        assertThat(seat1b.getCustomer().getFirstname(), is(customer3.getFirstname()));
    }
    @Test
    public void reserveAlreadyBookedSeatReturnsFalse() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.reserve(seat1a, customer3);

        // then
        assertThat(success, is(false));
        assertThat(seat1a.getCustomer(), is(customer1));
    }
    @Test
    public void bookSeatForUnknownCustomerReturnsFalse() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.reserve(seat1b, prepareCustomer("Lastname", "Firstname", "email@email.com"));

        // then
        assertThat(success, is(false));
        assertThat(seat1b.getCustomer(), is(nullValue()));

        // and when
        success = flightsDao.reserve(seat1b, null);

        // then
        assertThat(success, is(false));
    }
    @Test
    public void reserveNullAsSeatReturnsFalse() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.reserve(null, customer3);

        // then
        assertThat(success, is(false));

        // and when
        success = flightsDao.reserve(null, null);

        // then
        assertThat(success, is(false));
    }
    @Test
    public void reserveNotYetPersistedSeatForCustomerReturnsFalse() {
        // given
        setUpTestData();
        Seat newSeat = prepareSeat("17B", SeatType.MIDDLE);
        newSeat.setFlight(flight3);

        // when
        boolean success = flightsDao.reserve(newSeat, customer3);

        // then
        assertThat(success, is(false));
        assertThat(newSeat.getCustomer(), is(nullValue()));
    }
    // </editor-fold>

    // <editor-fold desc="cancel">
    @Test
    public void cancelAssignsNullAsCustomerToSeatAndReturnsTrue() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.cancel(seat1a, customer1);

        // then
        assertThat(success, is(true));
        assertThat(seat1a.getCustomer(), is(nullValue()));
    }
    @Test
    public void cancelAvailableSeatReturnsFalse() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.cancel(seat1b, customer3);

        // then
        assertThat(success, is(false));
        assertThat(seat1b.getCustomer(), is(nullValue()));
    }
    @Test
    public void cancelSeatForUnknownCustomerReturnsFalse() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.cancel(seat1a, prepareCustomer("Lastname", "Firstname", "email@email.com"));

        // then
        assertThat(success, is(false));
        assertThat(seat1a.getCustomer(), is(customer1));

        // and when
        success = flightsDao.cancel(seat1b, null);

        // then
        assertThat(success, is(false));
    }
    @Test
    public void cancelNullAsSeatReturnsFalse() {
        // given
        setUpTestData();

        // when
        boolean success = flightsDao.cancel(null, customer3);

        // then
        assertThat(success, is(false));

        // and when
        success = flightsDao.reserve(null, null);

        // then
        assertThat(success, is(false));
    }
    @Test
    public void cancelNotYetPersistedSeatForCustomerReturnsFalse() {
        // given
        setUpTestData();
        Seat newSeat = prepareSeat("17B", SeatType.MIDDLE);
        newSeat.setFlight(flight3);

        // when
        boolean success = flightsDao.cancel(newSeat, customer3);

        // then
        assertThat(success, is(false));
        assertThat(newSeat.getCustomer(), is(nullValue()));
    }
}
