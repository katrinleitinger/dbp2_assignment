package at.campus02.dbp2.flights.domain;

import javax.persistence.*;

@Entity
public class Seat {

    @Id @GeneratedValue
    private Integer id;
    private String seatNumber;
    private SeatType type;
    @ManyToOne (cascade = CascadeType.PERSIST)
    private Flight flight;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Customer customer;

    public Integer getId() {
        return id;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public SeatType getType() {
        return type;
    }

    public void setType(SeatType type) {
        this.type = type;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seat seat = (Seat) o;

        return id.equals(seat.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
