package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;
import at.campus02.dbp2.flights.domain.SeatType;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class TrickyCrudSpecification extends BaseSpecification {

    @Test
    public void deleteCustomerAlsoRemovesReferencesFromSeats() {
        // given
        Customer customer = prepareCustomer(lastname, firstname, email);
        flightsDao.create(customer);
        Flight flight = prepareFlight(flightNr1, graz);
        Seat occupied = prepareSeat("12A", SeatType.WINDOW);
        occupied.setCustomer(customer);
        flight.getSeats().add(occupied);
        occupied.setFlight(flight);
        flightsDao.create(flight);

        // when
        boolean success = flightsDao.delete(customer);

        // then
        assertThat(success, is(true));
        assertThat(entityManager.find(Seat.class, occupied.getId()), is(not(nullValue())));
        assertThat(entityManager.find(Seat.class, occupied.getId()).getCustomer(), is(nullValue()));
    }

    @Test
    public void deleteFlightAlsoDeletesSeatsInDatabase() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        Seat seat = prepareSeat("12A", SeatType.WINDOW);
        flight.getSeats().add(seat);
        seat.setFlight(flight);
        flightsDao.create(flight);

        // when
        boolean success = flightsDao.delete(flight);

        // then
        assertThat(success, is(true));
        assertThat(entityManager.find(Seat.class, seat.getId()), is(nullValue()));
    }

    @Test
    public void updateNotExistingFlightReturnsNull() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);

        // when
        Flight updated = flightsDao.update(flight);

        // then
        assertThat(updated, is(nullValue()));

        // and when
        flight = null;
        updated = flightsDao.update(flight);
        assertThat(updated, is(nullValue()));
    }

    @Test
    public void updateFlightWithTheSameSeatTwiceEndsUpHavingThatSeatOnlyOnceInList() {
        // given
        Flight car = prepareFlight(flightNr1, "Graz");
        Seat seat1 = prepareSeat("12A", SeatType.WINDOW);
        car.getSeats().add(seat1);
        flightsDao.create(car);

        // when
        car.getSeats().add(seat1);
        Flight updated = flightsDao.update(car);

        // then
        assertThat(updated, is(car));
        assertThat(updated.getSeats().size(), is(1));
        assertThat(updated.getSeats().contains(seat1), is(true));
        assertThat(seat1.getId(), is(not(nullValue())));
        assertThat(seat1.getFlight(), is(car));
    }

    // FIXME: zu schwer?
    @Test
    public void updateFlightDeletesAllSeatsNoLongerAssigned() {
        // given
        Flight flight = prepareFlight(flightNr1, graz);
        Seat seat1 = prepareSeat("12A", SeatType.WINDOW);
        Seat seat2 = prepareSeat("14C", SeatType.AISLE);
        Seat seat3 = prepareSeat("17E", SeatType.MIDDLE);
        flight.getSeats().add(seat1);
        flight.getSeats().add(seat2);
        flight.getSeats().add(seat3);
        flightsDao.create(flight);

        // when
        flight.getSeats().remove(seat1);
        flight.getSeats().remove(seat3);
        Flight updated = flightsDao.update(flight);

        // then
        assertThat(updated, is(flight));
        assertThat(updated.getSeats().size(), is(1));
        assertThat(updated.getSeats().contains(seat2), is(true));
        assertThat(entityManager.find(Seat.class, seat1.getId()), is(nullValue()));
        assertThat(entityManager.find(Seat.class, seat2.getId()), is(seat2));
        assertThat(entityManager.find(Seat.class, seat3.getId()), is(nullValue()));
    }

}
