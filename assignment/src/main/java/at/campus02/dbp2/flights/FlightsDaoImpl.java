package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class FlightsDaoImpl implements FlightsDao {

    private EntityManagerFactory factory;
    private EntityManager manager;

    public FlightsDaoImpl(EntityManagerFactory factory) {

        this.factory = factory;
        manager = factory.createEntityManager();
    }

    @Override
    public boolean create(Flight flight) {
        //Flight in DB anlegen

        //wenn der flight null ist
        if(flight == null){
            return false;
        }
        //wenn der flight noch nicht in der DB existiert
        else if(manager.find(Flight.class, flight.getFlightNumber()) == null){

            manager.getTransaction().begin();
            for (Seat s: flight.getSeats()
                 ) {
                s.setFlight(flight);
            }
            manager.persist(flight);
            manager.getTransaction().commit();




            //checken, ob flight in der DB ist
            if (manager.contains(manager.find(Flight.class, flight.getFlightNumber()))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Flight readFlight(String flightNumber) {

        if(flightNumber == null){
            return null;
        }
        return manager.find(Flight.class, flightNumber);
    }

    @Override
    public Flight update(Flight flight) {

        //muss vorher abgefangen werden, sonst gibt's eine null pointer exception
        if(flight == null
                //checken ob flight in der db existiert
                || manager.find(Flight.class, flight.getFlightNumber()) == null){
            return null;
        }


            manager.getTransaction().begin();

            for (Seat s : flight.getSeats()
            ){

                    s.setFlight(flight);
            }
            manager.getTransaction().commit();


            return manager.find(Flight.class, flight.getFlightNumber());



    }

    @Override
    public boolean delete(Flight flight) {

        if(flight.getFlightNumber() == null || manager.find(Flight.class, flight.getFlightNumber()) == null ){
            return false;
        }
        manager.getTransaction().begin();
        manager.remove(flight);
        manager.getTransaction().commit();

        if(manager.find(Flight.class, flight.getFlightNumber()) == null){
            return true;
        }

        return false;
    }

    @Override
    public boolean create(Customer customer) {
        //Customer in DB anlegen

        //wenn der customer null ist
        //oder die email vom customer null ist, der aber vor- und/oder nachname hat
        if(customer == null || customer.getEmail() == null){
            return false;
        }
        //wenn die email noch nicht in der DB existiert
        else if(manager.find(Customer.class, customer.getEmail()) == null){
            manager.getTransaction().begin();
            manager.persist(customer);
            manager.getTransaction().commit();
            //checken, ob Customer in der DB ist
            if (manager.contains(manager.find(Customer.class, customer.getEmail()))) {
                return true;
            }
        }
        return false;
    }


    @Override
    public Customer readCustomer(String email) {
        //wenn email null ist
        if(email == null){
            return null;
        }
        //sonst entsprechenden customer retour geben
        return manager.find(Customer.class, email);
    }

    @Override
    public Customer update(Customer customer) {

        if(customer == null){
            return null;
        }
        manager.getTransaction().begin();
        manager.getTransaction().commit();
        return manager.find(Customer.class, customer.getEmail());
    }

    @Override
    public boolean delete(Customer customer) {
        //customer aus DB entfernen
        //falls customer keine e-mail hat (wichtig!!! weil ohne das geht der test nicht) oder customer in DB nicht existiert -> false
        if(customer.getEmail() == null || manager.find(Customer.class, customer.getEmail()) == null){
            return false;
        }

        List<Seat> seatsForCustomer = manager.createQuery("select s from Seat s where s.customer = :customer").setParameter("customer", customer).getResultList();

        manager.getTransaction().begin();
        for (Seat s : seatsForCustomer
             ) {
            s.setCustomer(null);
        }
        manager.remove(customer);
        manager.getTransaction().commit();

        //checken ob customer auch wirklich nicht mehr in der DB ist
        //wenn customer nicht mehr in der DB ist -> true
        if (manager.find(Customer.class, customer.getEmail()) == null) {
            return true;
        }
        return false;
    }

    @Override
    public List<Flight> findFlightsByDestination(String destination) {

        if(destination == null){
            return manager.createNamedQuery("Flight.findAllFlights", Flight.class).getResultList();
        }
        else{
            return manager.createQuery("select f from Flight f where lower(f.destination) = :destination").setParameter
                    ("destination", destination.toLowerCase()).getResultList();
        }

    }

    @Override
    public List<Customer> findCustomersBy(String lastname, String firstname) {

        if(firstname != null && lastname != null) {
            return manager.createQuery("select c from Customer c where lower(c.lastname) = :lastname and lower(c.firstname) = :firstname").setParameter
                    ("lastname", lastname.toLowerCase()).setParameter("firstname", firstname.toLowerCase()).getResultList();
        }
        else if(firstname == null && lastname == null){
            return manager.createQuery("select c from Customer c").getResultList();
        }
        else if(firstname == null && lastname != null){
            return manager.createQuery("select c from Customer c where lower(c.lastname) = :lastname").setParameter
                    ("lastname", lastname.toLowerCase()).getResultList();
        }
        else{
            return manager.createQuery("select c from Customer c where lower(c.firstname) = :firstname").
                    setParameter("firstname", firstname.toLowerCase()).getResultList();
        }
    }

    @Override
    public List<Seat> findAvailableSeatsTo(String destination) {

        if(destination == null){
            return manager.createQuery("select s from Seat s where s.customer is null").getResultList();
        }

        return manager.createQuery("select s from Seat s where lower(s.flight.destination) = :destination and s.customer is null").setParameter
                ("destination", destination.toLowerCase()).getResultList();
    }

    @Override
    public List<Seat> findSeatsBookedBy(Customer customer) {
        return manager.createQuery("select s from Seat s where s.customer = :customer").setParameter
                ("customer", customer).getResultList();
    }

    @Override
    public boolean reserve(Seat seat, Customer customer) {

        if(customer == null || seat == null
                || seat.getId() == null
                || seat.getFlight() == null
                || manager.find(Customer.class, customer.getEmail()) == null
                || manager.find(Seat.class, seat.getId()).getCustomer() != null
        ){
            return false;
        }
        else {
            manager.getTransaction().begin();
            seat.setCustomer(customer);
            manager.getTransaction().commit();
            if (seat.getCustomer().equals(customer)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean cancel(Seat seat, Customer customer) {

        if(customer == null || seat == null
                || seat.getId() == null
                || seat.getFlight() == null
                || manager.find(Customer.class, customer.getEmail()) == null
                || manager.find(Seat.class, seat.getId()).getCustomer() == null
        ){
            return false;
        }
        else {
            manager.getTransaction().begin();
            seat.setCustomer(null);
            manager.getTransaction().commit();
            if (seat.getCustomer() == null) {
                return true;
            }
        }
        return false;

    }

    @Override
    public void close() {
        if(manager != null && manager.isOpen()){
            manager.close();
        }
    }
}

