package at.campus02.dbp2.flights;

import javax.persistence.EntityManagerFactory;

public class FlightsDaoFactory {

    private EntityManagerFactory factory;

    public FlightsDaoFactory(EntityManagerFactory factory) {
        this.factory = factory;
    }

    public FlightsDao getFlightsDao() {

        FlightsDaoImpl impl = new FlightsDaoImpl(factory);

        return impl;
    }
}

