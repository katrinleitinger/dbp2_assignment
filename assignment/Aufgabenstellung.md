## Aufgabenstellung

Erstellen Sie ein einfaches Flug-Buchungs-System für die Reservierung von Sitzplätzen in verfügbaren Flügen. 


Die zur Verfügung stehenden Sitzplätze in verschiedenen Flügen können von registrierten Kunden gebucht (und storniert) werden.  
 
 

Dazu sollen die Entitäten ``Flight``, ``Customer`` und ``Seat`` mittels 
JPA in einer Datenbank verwaltet werden. 

Die Signaturen dieser Klassen sehen dabei (zumindest) wie folgt aus:

``` java
public class Flight {
    public String getFlightNumber() {...}
    public void setFlightNumber(String flightNumber) {...}
    public String getDestination() {...}
    public void setDestination(String destination) {...}
    public List<Seat> getSeats() {...}
}

public class Customer {
    public String getEmail() {...}
    public void setEmail(String email) {...}
    public String getLastname() {...}
    public void setLastname(String lastname) {...}
    public String getFirstname() {...}
    public void setFirstname(String firstname) {...}
}

public class Seat {
    public Integer getId() {...}
    public String getSeatNumber() {...}
    public void setSeatNumber(String seatNumber) {...}
    public SeatType getType() {...}
    public void setType(SeatType type) {...}
    public Flight getFlight() {...}
    public void setFlight(Flight flight) {...}
    public Customer getCustomer() {...}
    public void setCustomer(Customer customer) {...}
}
```

Dabei ist der ``SeatType`` wie folgt definiert:

``` java
public enum SeatType {
    AISLE,
    MIDDLE,
    WINDOW
}
```

Die "Geschäftslogik" der Applikation wird dabei durch das vorgegebene Interface ``FlightDao`` beschrieben:

``` java
public interface FlightsDao {

    // CRUD: flights
    boolean create(Flight flight);
    Flight readFlight(String flightNumber);
    Flight update(Flight flight);
    boolean delete(Flight flight);

    // CRUD: customers
    boolean create(Customer customer);
    Customer readCustomer(String email);
    Customer update(Customer customer);
    boolean delete(Customer customer);

    // Suche: flights
    List<Flight> findFlightsByDestination(String destination);

    // Suche: customers
    List<Customer> findCustomersBy(String lastname, String firstname);

    // Suche: seats
    List<Seat> findAvailableSeatsTo(String destination);
    List<Seat> findSeatsBookedBy(Customer customer);

    // Reservierung / Cancel
    boolean reserve(Seat seat, Customer customer);
    boolean cancel(Seat seat, Customer customer);

    // Resource management
    void close();

}
``` 

Für die Erzeugung der tatsächlichen Implementierungen dieser Interfaces ist die folgende Factory vorgegeben:

``` java
public class FlightsDaoFactory {
    public FlightsDaoFactory(EntityManagerFactory factory) {...}
    public FlightsDao getFlightsDao() {...}
}
```

Die Spezifikation der Funktionalität erfolgt über vorgegebene Test-Klassen (``CrudSpecification`` und ``FlightsSpecification``, die etwas schwieriger umzusetzenden Funktionalitäten über ``TrickyCrudSpecification`` und ``TrickyFlightSpecification``).
Um alle vorhandenen Tests in einem Schritt laufen zu lassen, verwenden Sie die Klasse ``TestSuite``.

### Aufgaben

* Verwenden Sie dieses gradle-Projekt für die Implementierung der Aufgabe.
    * Öffnen Sie das Projekt in IntelliJ, indem Sie das ``build.gradle`` File als Projekt öffnen. 
    * Konfigurieren Sie die nötige _Persistence Unit_ für das Pojekt. Der Name der Persistence-Unit lautet dabei **"assignment"**.  
* Die Rümpfe der Entitäten finden Sie im Package ``at.campus02.dbp2.flights.domain``.
    * Stellen Sie die Entitäts-Klassen fertig und verwenden Sie dabei die nötigen JPA Mappings. 
    * **Achtung:** Implementieren/Überschreiben Sie die Methoden ``hashCode`` und ``equals``. Für die Tests genügt dabei, dass für den Vergleich nur der "PrimaryKey" verwendet wird. (Die Tests gehen davon aus, dass zB. das Vorhandensein einer Entität in einer Liste mittels ``contains(entity)`` abgefragt werden kann). 
    * Definieren Sie auf zumindest einer der Entitäten zumindest eine ``NamedQuery``, welche bei der Implementierung von ``FlightsDao`` verwendet wird.
* Das Interface ``FlightsDao`` finden Sie im Package ``at.campus02.dbp2.flights``.
    * Programmieren Sie eine Implementierung des ``FlightsDao`` Interfaces. 
    * Stellen Sie die ``ManagerFactory`` so fertig, dass die Factory-Methode eine Instanz Ihrer Implementierung von ``FlightsDao`` zurückgibt. 
* Die Spezifikation der Funktionalität ist über Testklassen vorgegeben. Sie finden diese Klassen im Source-Folder ``src/main/test`` im Package ``at.campus02.dbp2.flights``.
    * Implementieren Sie ``FlightsDao`` so, dass möglichst viele Unit-Tests  erfolgreich durchlaufen. 
    * Lassen Sie dann auch den ``IntegrationTest`` laufen und kontrollieren Sie in Ihrer Datenbank, ob die vorhandenen Objekte dem entsprechen, was am Ende von ``IntegrationTest`` als Kommentar beschrieben ist. 
    * Die vorgegebenen Testklassen sollen nicht geändert werden. Wenn Sie zusätzliche Tests schreiben, erstellen Sie dazu eine eigene Testklasse. 
* Kümmern Sie sich bei der Implementierung auch um die verwendeten Ressourcen (es gibt einen eigenen Test dafür). 


    
### Abgabe

Zippen Sie das fertiggestellte Projekt (den  gesamten Folder) und geben Sie dem zip-File den Namen "dbp2.IHRNAME.zip" (beispielsweise "dbp2.mair.zip"). 
Das zip-File kann entweder über Moodle abgegeben werden oder beim Klausur-Termin auf einem USB-Stick mitgebracht werden (die genauen Modalitäten besprechen wir im Kurs).

### Tipps zur Umsetzung

* Folgende Reihenfolge sollte Ihnen die Umsetzung erleichtern:

    1. Projekt öffnen (``build.gradle``) und ``TestSuite`` laufen lassen. Zunächst werden alle Tests rot sein, aber Sie wissen, dass das Projekt kompiliert und die Libraries geladen werden. Verwenden Sie auch das JPA-Facet, wenn IntelliJ Sie darauf hinweist, das erleichtert das Schreiben der Queries. 
    2. ``persistence.xml`` fertigstellen. Am besten ``drop-and-create-tables`` verwenden, damit die Datenbank bei jedem Test neu befüllt wird. 
    3. Entities fertigstellen (``Customer``, ``Flight``, ``Seat``). Das Verhindern von ``null`` für List-Attribute erspart Ihnen ``NullPointerExceptions``. 
        * Implementieren Sie auch gleich ``hashCode`` und ``equals`` für die Entity-Klassen und verwenden Sie dafür nur den jeweiligen Primary Key. 
    4. Implementieren Sie das ``FlightsDao``Interface in einer eigenen Klasse im selben Package, erstmal nur die Rümpfe der Methoden (default-Implementierung).
    5. Stellen Sie die ``FlightsDaoFactory`` so fertig, dass die Factory-Methode eine Instanz Ihrer Implementierung von ``FlightsDao`` zurückgibt.
    6. Lassen Sie wieder die ``TestSuite`` laufen. Einige der Tests sollten vorerst schon erfolgreich durchlaufen, wenn alle Schritte bisher korrekt umgesetzt wurden. Falls nicht, kontrollieren Sie, ob ``persistence.xml`` stimmig ist. 
    7. Beginnen Sie die Tests mit ``CrudSpecification``.
        * Am einfachsten arbeiten Sie die Tests der Reihe nach ab (wie im File vorhanden). 
        * Erweitern Sie Ihre Implementierung von ``FlightsDao`` Schritt für Schritt, bis möglichst alle Unit-Tests erfolgreich durchlaufen. 
        * Lassen Sie zwischendurch immer wieder mal alle Tests aus ``Crud-Specification`` laufen, um sicherzustellen, dass Änderungen keine bereits implementierte Funktionalität kaputt machen.
    8. Fahren Sie mit ``FlightsSpecification`` fort, bis auch dort möglichst alle Unit-Tests erfolgreich sind. 
    9. Fahren Sie mit ``TrickyFlightsSpecification`` fort. Wie der Name vermuten lässt, ist die Umsetzung etwas schwieriger. 
    10. Am Ende fahren Sie dann mit ``TrickyCrudSpecification`` fort. Die am schwierigsten umzusetzende Funktionalität wird mit dem Unit-Test ``updateFlightDeletesAllSeatsNoLongerAssigned`` abgeprüft. 
    11. Spätestens, wenn Sie mit dem Ergebnis zufrieden sind, lassen Sie auch den ``IntegrationTest`` laufen und kontrollieren Sie den Zustand Ihrer Datenbank. 

* Sollten Sie einmal gar nicht weiterkommen, hier noch ein paar Hinweise, die Sie eventuell auf die richtige Spur bringen (auch wenn sie zu Beginn vielleicht etwas kryptisch klingen):
    * Stimmen die Listen-Elemente in den Tests nicht oder ein Vergleich mit ``is()``, kontrollieren Sie auch ``hashCode`` und ``equals``. 
    * Sie können in JPQL auch ``like`` usw. verwenden. 
    * Die Relationen in der Datenbank werden von JPA verwaltet, im Speicher müssen Sie sich darum kümmern. Mit zB. einem ``refresh`` (``EntityManager``) lesen Sie aber die Werte aus der Datenbank neu ein. 
    * ``CascadeType`` für Relationen überlegen. 
    * Verwenden Sie eher Listen als Sets für gemappte Relationen (Sets können unerwünschte Nebeneffekte haben, wenn sich zB. der HashCode eines Objektes ändert). 
         